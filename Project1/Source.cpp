/*
Strudent Name : Abd Alrahman Hamdan
Registration Number : 11613083
First Artificial Intelligence HomeWork
Dr.Wael Mustafa

Note 1 : Iam use Open List As A Queue But It's Implemanted As A Linked List (First In First Out)
Note 2 : You can use any number to boat capacity
Note 3 : This algorithm can solve any problem , if the problem can't be solved the program will print can't be solved
Note 4 : This Program will print the execution time
Note 5 : To input our case (initial state : MMMMCCCCB|- , boat capacity 3 , final state : -|MMMMCCCCB)  you must input :
		 Boat capacity : 3
		 Initial state left side : MMMMCCCCB (capital case)
		 Initial state right side : just press enter (keep it empty)
		 Final state left side : just press enter (keep it empty)
		 Final state right side : MMMMCCCCB (capital case)
		 the program will print the solution : 
		 The solution is : 

		MMMMCCCCB |  -
		MMMMCC | CCB
		MMMMCCCB | C
		MMMM | CCCCB
		MMMMCB | CCC
		MC | MMMCCCB
		MMCCB | MMCC
		CC | MMMMCCB
		CCCB | MMMMC
		-  | MMMMCCCCB
		And It's Required 9 Boat Traviels
		
		Then the program will print the required time 


Note 6 : I will put a screen shot in the paiper for the  solution
Note 7 : I will work on algorithm time to make it better

*/

#include <iostream>
#include <string>
#include <stack>
#include <time.h>
using namespace std;

class Node {
private :

	string LeftSide;
	string RightSide;
	Node * Parent;
	Node * Next;

public :

	Node();
	void SetRightSide(string);
	void SetLeftSide(string);
	void SetNext(Node*);
	void SetParent(Node *);
	string GetLeftSide();
	string GetRightSide();
	Node * GetParent();
	Node * GetNext();

		   };//Node

Node::Node() : Parent(NULL),Next(NULL) {

	LeftSide = RightSide =  "";
	
									    }//Node

void Node::SetLeftSide(string Temp) {
	LeftSide = Temp;
									}//SetLeftSide

void Node::SetRightSide(string Temp) {
	RightSide = Temp;
									 }//SetRightSide

void Node::SetNext(Node * Temp) {
	Next = Temp;
								}//SetNext

void Node::SetParent(Node * Temp) {
	Parent = Temp;
							      }//SetParent

string Node::GetLeftSide() {
	return LeftSide;
						   }//GetData

string Node::GetRightSide() {
	return RightSide;
							}//GetRightSide

Node * Node::GetParent() {
	return Parent;
						 }//GetParent

Node * Node::GetNext() {
	return Next;
					   }//GetNext

class LinkedList {
private:

	Node * Head;
	
public :

	LinkedList();
	void AddAtEnd(Node *);
	int Search(string, string);
	void RemoveFront();
	int IsEmpty();
	Node * GetFirst();
	void MoveHead();

				 };//LinkedList

LinkedList::LinkedList() : Head(NULL) {
									  }//LinkedList

void LinkedList::AddAtEnd(Node * Temp) {

	Node * PTR = Head;
	if (PTR == NULL)
		Head = Temp;

	else {
		for (; PTR->GetNext() != NULL; PTR = PTR->GetNext());
		PTR->SetNext(Temp);
	
		 }//else

									   }//AddAtEnd

int LinkedList::Search(string Left , string Right) {

	Node * PTR = Head;
	if (PTR == NULL)
		return 0;

	else {
		for (; PTR != NULL; PTR = PTR->GetNext()) {
			if (PTR->GetLeftSide() == Left && PTR->GetRightSide() == Right)
				return 1;
															 }//for loop
		return 0;
		 }//else
									   }//Search

void LinkedList::RemoveFront() {
	Node * PTR = Head;
	if (PTR == NULL)
		return;

	else {
		Head = Head->GetNext();
		delete PTR;
		return;
		 }//else
							   }//RemoveFront

void LinkedList::MoveHead() {
	Node * PTR = Head;
	if (PTR == NULL)
		return;

	else {
		Head = Head->GetNext();
		return;
		 }//else
						    }//MoveHead

int LinkedList::IsEmpty() {
	return Head == NULL;
						  }//IsEmpty

Node * LinkedList::GetFirst() {
	return Head;
							  }//GetFirst

void Expand(Node*, LinkedList&,LinkedList,int);

Node * Move(Node *, string);

int CountainBoat(string);

int CharCounter(string , char);

void main(void) {

	LinkedList Open;
	LinkedList Closed;
	Node * InitialState = new Node();
	Node * N = NULL;

	int BoatCapacity;
	cout << " Please Enter Boat Capacity : ";
	cin >> BoatCapacity;

	char * Temp = new char[1000];
	cin.ignore();
	cout << " Please Enter Initial State Left Side : ";
	cin.getline(Temp, 1000);

	InitialState->SetLeftSide(Temp);

	int InitialStateLeftMCounter = CharCounter(InitialState->GetLeftSide(), 'M');

	int InitialStateLeftCCounter = CharCounter(InitialState->GetLeftSide(), 'C');

	if (InitialStateLeftCCounter > InitialStateLeftMCounter && InitialStateLeftMCounter != 0) {
		system("CLS");
		cout << "Can't Solve This Problem ! , because in the left side of initial state the number of C is bigger than the number of M" << endl;
		system("pause");
		exit(-1);
																							  }//if statment

	cout << " Please Enter Initial State Right Side : ";
	cin.getline(Temp, 1000);
	InitialState->SetRightSide(Temp);

	int InitialStateRightMCounter = CharCounter(InitialState->GetRightSide(), 'M');

	int InitialStateRightCCounter = CharCounter(InitialState->GetRightSide(), 'C');

	if (InitialStateRightCCounter > InitialStateRightMCounter && InitialStateRightMCounter != 0) {
		system("CLS");
		cout << "Can't Solve This Problem ! , because in the right side of initial  state the number of C is bigger than the number of M" << endl;
		system("pause");
		exit(-1);
																								 }//if statment

	Open.AddAtEnd(InitialState);

	stack <string> Stack;
	
	char * GoalStateLeftSide = new char[1000];
	char * GoalStateRightSide = new char[1000];

	cout << " Please Enter Goal State Left Side : ";
	cin.getline(GoalStateLeftSide, 1000);

	int GoalStateLeftMCounter = CharCounter(GoalStateLeftSide, 'M');

	int GoalStateLeftCCounter = CharCounter(GoalStateLeftSide, 'C');

	if (GoalStateLeftCCounter > GoalStateLeftMCounter && GoalStateLeftMCounter != 0) {
		system("CLS");
		cout << "Can't Solve This Problem ! , because in the left side of goal state the number of C is bigger than the number of M" << endl;
		system("pause");
		exit(-1);
																					 }//if statment

	cout << " Please Enter Goal State Right Side : ";
	cin.getline(GoalStateRightSide, 1000);

	int GoalStateRightMCounter = CharCounter(GoalStateRightSide, 'M');

	int GoalStateRightCCounter = CharCounter(GoalStateRightSide, 'C');

	if (GoalStateRightCCounter > GoalStateRightMCounter && GoalStateRightMCounter != 0) {
		system("CLS");
		cout << "Can't Solve This Problem ! , because in the right side of  goal state the number of C is bigger than the number of M" << endl;
		system("pause");
		exit(-1);
																						}//if statment

	if (GoalStateLeftCCounter + GoalStateRightCCounter != InitialStateLeftCCounter + InitialStateRightCCounter
		|| GoalStateLeftMCounter + GoalStateRightMCounter != InitialStateLeftMCounter + InitialStateRightMCounter) {
		
		system("CLS");
		cout << "Can't Solve This Problem ! , because the number of M or C not equal in the initial state and in the goal state" << endl;
		system("pause");
		exit(-1);
																												   }//if statment
	

	clock_t Time = clock();

	system("CLS");

	while (!Open.IsEmpty()) {
		
		N = Open.GetFirst();
		if (Closed.Search(N->GetLeftSide(),N->GetRightSide()) == 0)
		Closed.AddAtEnd(N);
		
		if (strcmp(N->GetRightSide().c_str(), GoalStateRightSide)==0 /*&& N->GetLeftSide() == GoalStateLeftSide*/)
			break;

		Expand(N,Open,Closed,BoatCapacity);
		Open.MoveHead();

						    }//while loop

	Time = clock() - Time;
	
	if (Open.IsEmpty()) {
		cout << "Can't Solve This Problem !" << endl;
		system("pause");
		exit(-1);
						}// if statment

	else {

		cout << "The Soultion Is : " << endl;
		int i = 0;

		for (; N != NULL; N = N->GetParent(), i++) {
			if (N->GetRightSide() == "")
				Stack.push(" - ");

			else
				Stack.push(N->GetRightSide());

			Stack.push(" | ");

			if (N->GetLeftSide() == "")
				Stack.push(" - ");

			else
				Stack.push(N->GetLeftSide());

			Stack.push("\n");
			
													  }//for loop

		for (; !Stack.empty();) {
			cout << Stack.top();
			Stack.pop();
							    }//for loop

		cout << endl << " And It's Required " << i - 1 << " Boat Traviels " << endl;

		cout << " This Algorithm Reqauires " << ((float)Time / CLOCKS_PER_SEC) << " Second To Finish !" << endl;

		system("pause");

		 }//else

				}//main

void Expand(Node * Temp, LinkedList &Open,LinkedList Closed,int Capacity) {

	Node * Try;

	string Temporary;

	for (int i = 0 ; i < Capacity; i++) {
		
		Temporary.resize(i+1);
		Temporary[i] = 'M';
		Try = Move(Temp, Temporary);
		if (Try != NULL)
			if (Closed.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0 && Open.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0)
				Open.AddAtEnd(Try);
									   }//for loop
	
	string Tempo;

	for (int i = 0; i < Capacity; i++) {
		Tempo.resize(i+1);
		Tempo[i] = 'C';
		Try = Move(Temp, Tempo);
		if (Try != NULL)
			if (Closed.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0 && Open.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0)
				Open.AddAtEnd(Try);
									   }//for loop

	if (Capacity % 2 == 0)
	for (int i = 0; i < Capacity-1; i++) {
		for (int k = 0 ; k <= i ; k++)
			Temporary[k] = 'M';
			  for (int j = i+1; j < Capacity; j++) {
				 Temporary.resize(i+j+1);
				 Temporary[j] = 'C';
				 Try = Move(Temp, Temporary);
				 if (Try != NULL)
					 if (Closed.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0 && Open.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0)
						 Open.AddAtEnd(Try);
													  }//third for loop
									      }//first for loop
	else {
		for (int i = 0; i < Capacity-1; i++) {
		for (int k = 0 ; k <= i ; k++)
			Temporary[k] = 'M';
			  for (int j = i+1; j < Capacity-1; j++) {
				 Temporary.resize(i+j+1);
				 Temporary[j] = 'C';
				 Try = Move(Temp, Temporary);
				 if (Try != NULL)
					 if (Closed.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0 && Open.Search(Try->GetLeftSide(), Try->GetRightSide()) == 0)
						 Open.AddAtEnd(Try);
													  }//third for loop
									      }//first for loop
		 }//else


										  }//Expand

Node * Move(Node * Temp, string Operation) {

	string Left = Temp->GetLeftSide();
	string Right = Temp->GetRightSide();


	if (CountainBoat(Left)) {

		int LeftMCounter = 0;
		for (int i = 0;i < Left.length() && Left[i] != 'C'; i++)
			LeftMCounter++;

		int LeftCCounter = (Left.length() -1) - LeftMCounter;

		int MCounterForOperation = 0;
		for (int i = 0; i < Operation.length() && Operation[i] != 'C'; i++)
			MCounterForOperation++;

		int CCounterForOperation = Operation.length() - MCounterForOperation;

		if (CCounterForOperation > LeftCCounter || MCounterForOperation > LeftMCounter)
			return NULL;

		LeftMCounter -= MCounterForOperation;
		LeftCCounter -= CCounterForOperation;

		if (LeftCCounter > LeftMCounter && LeftMCounter != 0)
			return NULL;

		else {
			
			int RightMCounter = 0;
			for (int i = 0; i < Right.length() && Right[i] != 'C'; i++)
				RightMCounter++;

			int RightCCounter = Right.length() - RightMCounter;

			RightMCounter += MCounterForOperation;
			RightCCounter += CCounterForOperation;

			if (RightCCounter > RightMCounter && RightMCounter != 0)
				return NULL;

			else {

				Left.resize(LeftCCounter + LeftMCounter, ' ');
				Right.resize(RightCCounter + RightMCounter+1, ' ');
				
				for (int i = 0; i < LeftMCounter; i++)
					Left[i] = 'M';

				for (int i = LeftMCounter; i < LeftMCounter + LeftCCounter; i++)
					Left[i] = 'C';

				for (int i = 0; i < RightMCounter; i++)
					Right[i] = 'M';

				for (int i = RightMCounter; i < RightMCounter + RightCCounter; i++)
					Right[i] = 'C';

				Right[RightCCounter + RightMCounter] = 'B';

				Node * Try = new Node();
				Try->SetParent(Temp);
				Try->SetRightSide(Right);
				Try->SetLeftSide(Left);
				return Try;
				 }//else

			 }//else

							}//if statment

	else {

		int RightMCounter = 0;
		for (int i = 0; i<Right.length() && Right[i] != 'C'; i++)
			RightMCounter++;

		int RightCCounter = (Right.length() -1) - RightMCounter;

		int MCounterForOperation = 0;
		for (int i = 0; i < Operation.length() && Operation[i] != 'C'; i++)
			MCounterForOperation++;

		int CCounterForOperation = Operation.length() - MCounterForOperation;

		if (CCounterForOperation > RightCCounter || MCounterForOperation > RightMCounter)
			return NULL;

		RightMCounter -= MCounterForOperation;
		RightCCounter -= CCounterForOperation;

		if (RightCCounter > RightMCounter && RightMCounter != 0)
			return NULL;
		
		else {

			int LeftMCounter = 0;
			for (int i = 0;i<Left.length() && Left[i] != 'C'; i++)
				LeftMCounter++;

			int LeftCCounter = Left.length() - LeftMCounter;

			LeftMCounter += MCounterForOperation;
			LeftCCounter += CCounterForOperation;

			if (LeftCCounter > LeftMCounter && LeftMCounter != 0)
				return NULL;

			else {

				Left.resize(LeftCCounter + LeftMCounter+1, ' ');
				Right.resize(RightCCounter + RightMCounter, ' ');

				for (int i = 0; i < LeftMCounter; i++)
					Left[i] = 'M';

				for (int i = LeftMCounter; i < LeftMCounter + LeftCCounter; i++)
					Left[i] = 'C';

				for (int i = 0; i < RightMCounter; i++)
					Right[i] = 'M';

				for (int i = RightMCounter; i < RightMCounter + RightCCounter; i++)
					Right[i] = 'C';

				Left[LeftCCounter + LeftMCounter] = 'B';

				Node * Try = new Node();
				Try->SetParent(Temp);
				Try->SetRightSide(Right);
				Try->SetLeftSide(Left);
				return Try;

				 }//else

			 }//else

		 }//else


										   }//Move

int CountainBoat(string Temp) {

	if (Temp[Temp.length()-1] == 'B')
		return 1;

	return 0;
							  }//CountainBoat

int CharCounter(string Temp, char temp) {
	int Counter = 0;
	for (int i = 0; i < Temp.length();i++) {
		if (Temp[i] == temp)
			Counter++;
										   }//for loop
	return Counter;
										}//CharCounter